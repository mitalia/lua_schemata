return {
    workingarea={left=0, top=0, right=100, bottom=100},
    area_finestra_balenga={
        left=0, right=20, top=10, bottom=50
    },
    tools = {
        {
            _class = "blade",
            offset = {x=12, y=20},
            ortho_offset = 12,
            normal_offset = 12,
            angle_speed=10,
        },
        {
            _class = "shaped_punch",
            shape_desc = "star",
            normal_offset=1,
            ortho_offset=2
        },
        {
            _class = "shaped_punch",
            shape_desc = "circle",
            normal_offset=1,
            ortho_offset=-1
        },
        {
            _class = "multi_punch",
            diameters = {1, 3, 5.5, 18}
        }
    }
}
