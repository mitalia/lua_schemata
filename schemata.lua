inspect = require("inspect")

function dump(arg)
    print(inspect.inspect(arg))
end

function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else
        copy = orig
    end
    return copy
end


-- stack with the variable names currently examined - useful for backtraces
check_schema_stack = {}

-- returns the message prepended with the current state of the stack
function csm(msg)
    local path = ""
    for i,v in ipairs(check_schema_stack) do
        if i>1 then v = "." .. v end
        path = path .. v
    end
    return path .. ": " .. msg
end

-- throws with the current state of the stack
function cserror(msg)
    msg = csm(msg)
    check_schema_stack = {}
    error(msg)
end

-- checks if var conforms to schema; var_name is used only for debugging traces
function check_schema(var, schema, var_name)
    local function check_type(expected, var, display)
        if type(var)~=expected then
            cserror("Invalid type " .. type(var) .. ", expected " .. (display or expected))
        end
    end

    -- no schema => everything goes
    if not schema then return end

    -- push the current name on the stack
    check_schema_stack[#check_schema_stack+1] = var_name

    -- if no type is explicitly defined assume table
    local ttype = schema._type or 'table'

    -- limits for numeric ranges (=>number) or number of elements (arrays)
    local min, max
    if schema._meta then
        min = schema._meta.min
        max = schema._meta.max
    end

    if ttype == 'table' then
        -- basic type check
        check_type(ttype, var)

        -- recursively look for a subclass that matches the name
        local function find_subclass(schema, name)
            local subclasses = schema._subclasses
            if not subclasses then return nil end
            -- first look at the direct descendant
            local ret = subclasses[name]
            if ret then return ret end
            -- if we can't find anything, look deeper down
            for k,v in pairs(subclasses) do
                ret = find_subclass(v, name)
                if ret then return ret end
            end
            return nil
        end

        -- we do the subclass stuff only if someone requires it
        local subclasses = schema._subclasses
        if subclasses then
            -- a value that is required to be of a subclass type must
            -- exhibit a _class attribute, so we can check its validity
            local class = var._class
            if not class then cserror("Missing _class") end
            schema = find_subclass(schema, class)
            if not schema then cserror("Invalid _class") end
        end

        -- match the schema keys with the ones of the value
        for k,vschema in pairs(schema) do
            -- stuff starting with _ is for internal use,
            -- don't mess with it
            if string.sub(k,1,1)~="_" then
                local value = var[k]
                if not value then
                    -- not present
                    if vschema._meta and vschema._meta.default then
                        -- populate with the default if available
                        var[k] = deepcopy(vschema._meta.default)
                        value = var[k]
                    else
                        cserror("Missing value: " .. k)
                    end
                end
                -- recurse
                check_schema(value, vschema, k)
            end
        end
    elseif ttype == 'number' then
        check_type(ttype, var)
        -- range check
        if (min and var<min) or (max and var>max) then
            cserror(var .. " is out of range ([" .. (min  or '*') .. ", " .. (max or '*') .. "])")
        end
    elseif ttype == 'string' then
        check_type(ttype, var)
    elseif ttype == 'array' then
        -- we fake arrays in the usual Lua way
        check_type('table', var, 'array')
        -- for arrays, the range check is over the size
        if (min and #var<min) or (max and #var>max) then
            cserror("Array size " .. #var .. " is out of range ([" .. (min  or '*') .. ", " .. (max or '*') .. "])")
        end
        -- check the elements
        for i,v in ipairs(var) do
            check_schema(v, schema._arrtype, i)
        end
    end

    -- pop
    check_schema_stack[#check_schema_stack] = nil
end

-- shallow merge
function merge(t1, t2)
    local ret = {}
    for k,v in pairs(t1) do
        ret[k]=v
    end
    for k,v in pairs(t2) do
        ret[k]=v
    end
    return ret
end

-- subclass helper: adds the constraints taken from base to derived,
-- and adds the derived schema to the base subclasses
function subclass(name, base, derived)
    for k,v in pairs(base) do
        if k~="_subclasses" and not derived[k] then
            derived[k]=base[k]
        end
    end
    base._subclasses[name] = derived
end

-- helper for strings with custom metadata
function tstring(data)
    return {_type = "string", _meta = data}
end

-- helper for numbers with custom metadata
function number(data)
    return {_type = "number", _meta = data}
end

-- helper for arrays of a given type with custom metadata
function tarray(ttype, data)
    return {_type = "array", _arrtype = ttype, _meta = data}
end

-- a byte is a number with range [0, 255]
function byte(data)
    return number({desc=data.desc, min=0, max=255})
end

-- helper for a bidimensional point with custom metadata
function p2d(data)
    return {
        x = number(),
        y = number(),
        _meta = data
    }
end

-- rgb color
function color(data)
    return {
        _meta = data,
        r = byte({desc="red component"}),
        g = byte({desc="green component"}),
        b = byte({desc="blue component"})
    }
end

-- rectangle
function rect(data)
    return {
        _meta = data,
        left = number(),
        top = number(),
        right = number(),
        bottom = number(),
    }
end

-- base dictionary for all the tools
tool_base = {
    offset = p2d({default={x=0, y=0}}),
    _subclasses = {}
}

-- generates the schema for a generic tool with custom metadata
function tool(data)
    return merge(tool_base, {_meta = data})
end

-- multi_punch: inherits the tool attributes, adding itself to its subclasses
subclass("multi_punch", tool_base, {
    diameters = tarray(number({min=0}), {desc="Diameters of the installed punches"})
})

-- not a "real" class, just a factorization of the required fields for any rotating tool
rotating_tool = {
    ortho_offset = number({desc="Orthogonal offset"}),
    normal_offset = number({desc="Normal offset"})
}

-- shaped punch - we inherit from tool, adding the "rotating tool" fields and shape_desc
subclass("shaped_punch", tool_base, merge(rotating_tool, {
    shape_desc = tstring()
}))

-- same as above for the blade
subclass("blade", tool_base, merge(rotating_tool, {
    angle_speed = number({min=0, desc="Blade angle speed"})
}))

-- the actual schema
GLParmsSchema = {
    workingarea = rect({desc="working area of the machine", default={left=10, top=20, down=30, bottom=40}}),
    colorelineagialla = color({desc="Ovviamente il colore della linea gialla", default={r=30, g=40, b=50}}),
    cn_max_offset_aldo_antanizzato = number({min=12, max=6500, desc="boh", default=12}),
    area_finestra_balenga = {
        left = number({min=0, default=0}),
        top = number({min=0, default=0}),
        right = number({min=0, default=100}),
        bottom = number({min=0, default=100}),
        _meta = { desc = "Finestre aldo", default={} }
    },
    ultimi_difetti = tarray(p2d({desc="Difetti magici", default={x=50, y=20}}), {default={}, min=0, max=12}),
    tools = tarray(tool())
}

-- debug
dump(GLParmsSchema)

-- start empty
parms = {}

-- load the actual data
userParms = dofile("parms.lua")
-- merge (useless, but whatever)
parms = merge(parms, userParms)

print("----------------------------------------")
-- check the schema
check_schema(parms, GLParmsSchema, "parms")

-- dump the updated params
dump(parms)
